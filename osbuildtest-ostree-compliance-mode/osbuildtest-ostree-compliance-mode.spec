Name:           osbuildtest-ostree-compliance-mode
Version:        0.1
Release:        1%{?dist}
Summary:        Populates the ostree-compliance-mode.conf file with the checksum for the current booted deployment.

License:        GPLv2
Source0:        org.osbuildtest.ostree-compliance-mode.config

BuildArch:      noarch

%description
Populates the ostree-compliance-mode.conf file with the checksum for the current booted deployment.
This is required by the ostree-compliance-mode rpm which allows the system to move to a modifiable state,
in compliance with GPLv3


%prep
rm -rf %{name}-{%version}
mkdir %{name}-{%version}

%build
cd %{name}-{%version}

%install
install -Dm755 %{SOURCE0} %{buildroot}%{_prefix}/lib/osbuild/stages/org.osbuildtest.ostree-compliance-mode.config

%files
%{_prefix}/lib/osbuild/stages/org.osbuildtest.ostree-compliance-mode.config

%changelog
* Thu Feb 9 2023 Ian Mullins <imullins@redhat.com>
- Initial version
